﻿using System.Data.Entity.ModelConfiguration;
using TodoApp.Entity;

namespace TodoApp.Data.EntityTypeConfigurations
{
    public class EntityTypeConfigurationBase<T>
        where T : EntityBase
    {
        public EntityTypeConfigurationBase(EntityTypeConfiguration<T> configuration)
        {
        }
    }
}