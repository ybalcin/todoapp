﻿using System.Data.Entity.ModelConfiguration;
using TodoApp.Entity;

namespace TodoApp.Data.EntityTypeConfigurations
{
    public class TodoListItemEntityTypeConfiguration : EntityTypeConfigurationBase<TodoItem>
    {
        public TodoListItemEntityTypeConfiguration(EntityTypeConfiguration<TodoItem> configuration)
            : base(configuration)
        {
            configuration.HasKey(k => k.Id);

            configuration.Property(p => p.Subject).IsRequired();
            configuration.Property(p => p.HostAddress).IsRequired();
        }
    }
}