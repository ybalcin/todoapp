﻿using System.Data.Entity;
using TodoApp.Data.EntityTypeConfigurations;
using TodoApp.Entity;

namespace TodoApp.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            new TodoListItemEntityTypeConfiguration(modelBuilder.Entity<TodoItem>());
            base.OnModelCreating(modelBuilder);
        }
    }
}