﻿using TodoApp.Entity;

namespace TodoApp.Data.Repository.Abstract
{
    public interface ITodoItemRepository : IRepository<TodoItem>
    {
    }
}