﻿using System;
using System.Linq;
using TodoApp.Entity;

namespace TodoApp.Data.Repository.Abstract
{
    public interface IRepository<TEntity> : IDisposable where TEntity : EntityBase
    {
        void Add(TEntity entity);
        TEntity Get(Guid id);
        void Update(TEntity entity);
        void Remove(Guid id);
        IQueryable<TEntity> GetList();
    }
}