﻿using System;
using System.Data.Entity;
using System.Linq;
using TodoApp.Data.Context;
using TodoApp.Data.Repository.Abstract;
using TodoApp.Entity;

namespace TodoApp.Data.Repository.Concrete
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        protected readonly ApplicationDbContext Db;
        protected readonly DbSet<TEntity> DbSet;

        protected Repository(ApplicationDbContext context)
        {
            Db = context;
            DbSet = context.Set<TEntity>();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Add(TEntity entity)
        {
            entity.Id = Guid.NewGuid();
            DbSet.Add(entity);
            SaveChanges();
        }

        public TEntity Get(Guid id)
        {
            return DbSet.SingleOrDefault(f => f.Id == id && f.IsActive && !f.IsDeleted);
        }

        public virtual void Update(TEntity entity)
        {
            entity.UpdatedDate = DateTime.Now;
            Db.Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        public virtual void Remove(Guid id)
        {
            var entity = DbSet.SingleOrDefault(f => f.Id == id && f.IsActive && !f.IsDeleted);
            if (entity == null) return;
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTime.Now;
            Db.Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        public IQueryable<TEntity> GetList()
        {
            return DbSet.Where(f => f.IsActive && !f.IsDeleted);
        }

        protected int SaveChanges()
        {
            return Db.SaveChanges();
        }
    }
}