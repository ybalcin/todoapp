﻿using TodoApp.Data.Context;
using TodoApp.Data.Repository.Abstract;
using TodoApp.Entity;

namespace TodoApp.Data.Repository.Concrete
{
    public class TodoItemRepository : Repository<TodoItem>, ITodoItemRepository
    {
        public TodoItemRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}