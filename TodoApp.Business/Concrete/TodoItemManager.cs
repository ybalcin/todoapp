﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Business.Abstract;
using TodoApp.Core.Aspect;
using TodoApp.DataAccess.Abstract;
using TodoApp.Dto;
using TodoApp.Validation.Validators;

namespace TodoApp.Business.Concrete
{
    public class TodoItemManager : ITodoItemService
    {
        private readonly ITodoItemDal _itemDal;

        public TodoItemManager(ITodoItemDal itemDal)
        {
            _itemDal = itemDal;
        }

        [ValidationAspect(typeof(TodoItemAddValidator))]
        public void Add(TodoItemDto dto)
        {
            _itemDal.Add(dto);
        }

        public TodoItemDto Get(Guid id)
        {
            return _itemDal.Get(id);
        }

        [ValidationAspect(typeof(TodoItemAddValidator))]
        public void Update(TodoItemDto dto)
        {
            _itemDal.Update(dto);
        }

        public void Remove(Guid id)
        {
            _itemDal.Remove(id);
        }

        public IEnumerable<TodoItemDto> GetList()
        {
            return _itemDal.GetList()
                .OrderByDescending(o => o.PublishDate);
        }
    }
}