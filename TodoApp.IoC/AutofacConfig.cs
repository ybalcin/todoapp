﻿using Autofac;
using AutoMapper;
using TodoApp.DataAccess.Configuration.AutoMapper;
using TodoApp.IoC.Modules;

namespace TodoApp.IoC
{
    public static class AutofacConfig
    {
        public static void InitializeDependencies(this ContainerBuilder builder)
        {
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new DataAccessModule());
            builder.RegisterModule(new BusinessModule());

            builder.Register(ctx => AutoMapperConfig.RegisterMappings())
                .AsSelf().SingleInstance();

            builder.Register(c =>
                {
                    var ctx = c.Resolve<IComponentContext>();
                    var cfg = ctx.Resolve<MapperConfiguration>();
                    return cfg.CreateMapper(ctx.Resolve);
                })
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }
    }
}