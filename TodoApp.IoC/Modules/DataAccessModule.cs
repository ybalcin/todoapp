﻿using System;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;
using TodoApp.Core.Utility.Interceptor;
using TodoApp.DataAccess.Abstract;
using TodoApp.DataAccess.Concrete;

namespace TodoApp.IoC.Modules
{
    public class DataAccessModule : ModuleBase
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TodoItemDal>().As<ITodoItemDal>().InstancePerRequest();
            
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
                if (assembly.Location.Contains("TodoApp.DataAccess"))
                    builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces().EnableInterfaceInterceptors(
                        new ProxyGenerationOptions
                        {
                            Selector = new AspectInterceptorSelector()
                        }).InstancePerLifetimeScope();
        }
    }
}