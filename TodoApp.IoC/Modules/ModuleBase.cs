﻿using Autofac;

namespace TodoApp.IoC.Modules
{
    public abstract class ModuleBase : Module
    {
        protected abstract override void Load(ContainerBuilder builder);
    }
}