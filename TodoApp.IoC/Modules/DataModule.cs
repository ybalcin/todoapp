﻿using System;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;
using TodoApp.Core.Utility.Interceptor;
using TodoApp.Data.Context;
using TodoApp.Data.Repository.Abstract;
using TodoApp.Data.Repository.Concrete;

namespace TodoApp.IoC.Modules
{
    public class DataModule : ModuleBase
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(ApplicationDbContext)).InstancePerRequest();

            builder.RegisterType<TodoItemRepository>().As<ITodoItemRepository>().InstancePerRequest();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
                if (assembly.Location.Contains("TodoApp.Data"))
                    builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces().EnableInterfaceInterceptors(
                        new ProxyGenerationOptions
                        {
                            Selector = new AspectInterceptorSelector()
                        }).InstancePerLifetimeScope();
        }
    }
}