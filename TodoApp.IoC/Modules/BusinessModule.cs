﻿using System;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;
using TodoApp.Business.Abstract;
using TodoApp.Business.Concrete;
using TodoApp.Core.Utility.Interceptor;

namespace TodoApp.IoC.Modules
{
    public class BusinessModule : ModuleBase
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TodoItemManager>().As<ITodoItemService>().InstancePerRequest();
            
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
                if (assembly.Location.Contains("TodoApp.Business"))
                    builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces().EnableInterfaceInterceptors(
                        new ProxyGenerationOptions
                        {
                            Selector = new AspectInterceptorSelector()
                        }).InstancePerLifetimeScope();
        }
    }
}