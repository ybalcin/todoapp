﻿using System;

namespace TodoApp.Dto
{
    public class TodoItemDto
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Detail { get; set; }
        public string HostAddress { get; set; }
        public DateTime PublishDate { get; set; }
    }
}