﻿using AutoMapper;
using TodoApp.Dto;
using TodoApp.Entity;

namespace TodoApp.DataAccess.Configuration.AutoMapper
{
    public class EntityToDtoMappingProfile : Profile
    {
        public EntityToDtoMappingProfile()
        {
            CreateMap<TodoItem, TodoItemDto>();
        }
    }
}