﻿using AutoMapper;

namespace TodoApp.DataAccess.Configuration.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new EntityToDtoMappingProfile());
                cfg.AddProfile(new DtoToEntityMappingProfile());
            });
        }
    }
}