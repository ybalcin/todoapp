﻿using AutoMapper;
using TodoApp.Dto;
using TodoApp.Entity;

namespace TodoApp.DataAccess.Configuration.AutoMapper
{
    public class DtoToEntityMappingProfile : Profile
    {
        public DtoToEntityMappingProfile()
        {
            CreateMap<TodoItemDto, TodoItem>();
        }
    }
}