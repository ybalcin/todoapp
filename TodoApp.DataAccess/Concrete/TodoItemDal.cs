﻿using System;
using System.Collections.Generic;
using AutoMapper;
using TodoApp.Data.Repository.Abstract;
using TodoApp.DataAccess.Abstract;
using TodoApp.Dto;
using TodoApp.Entity;

namespace TodoApp.DataAccess.Concrete
{
    public class TodoItemDal : ITodoItemDal
    {
        private readonly ITodoItemRepository _itemRepository;
        private readonly IMapper _mapper;

        public TodoItemDal(ITodoItemRepository itemRepository, IMapper mapper)
        {
            _itemRepository = itemRepository;
            _mapper = mapper;
        }

        public void Add(TodoItemDto dto)
        {
            _itemRepository.Add(_mapper.Map<TodoItem>(dto));
        }

        public TodoItemDto Get(Guid id)
        {
            return _mapper.Map<TodoItemDto>(_itemRepository.Get(id));
        }

        public void Update(TodoItemDto dto)
        {
            _itemRepository.Update(_mapper.Map<TodoItem>(dto));
        }

        public void Remove(Guid id)
        {
            _itemRepository.Remove(id);
        }

        public IEnumerable<TodoItemDto> GetList()
        {
            return _mapper.ProjectTo<TodoItemDto>(_itemRepository.GetList());
        }
    }
}