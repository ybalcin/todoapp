﻿using System;
using System.Collections.Generic;
using TodoApp.Dto;

namespace TodoApp.DataAccess.Abstract
{
    public interface ITodoItemDal
    {
        void Add(TodoItemDto dto);
        TodoItemDto Get(Guid id);
        void Update(TodoItemDto dto);
        void Remove(Guid id);
        IEnumerable<TodoItemDto> GetList();
    }
}