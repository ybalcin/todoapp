﻿namespace TodoApp.Validation.Messages
{
    public struct CustomMessages
    {
        public const string NotEmpty = "Bu alan boş geçilemez!";
        public const string MustGreaterThanOrEqualNow = "Geçmiş tarih seçilemez!";
    }
}