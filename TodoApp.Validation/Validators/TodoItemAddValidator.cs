﻿using System;
using FluentValidation;
using TodoApp.Dto;
using TodoApp.Validation.Messages;

namespace TodoApp.Validation.Validators
{
    public class TodoItemAddValidator : AbstractValidator<TodoItemDto>
    {
        public TodoItemAddValidator()
        {
            RuleFor(r => r.Subject)
                .NotEmpty()
                .WithMessage(CustomMessages.NotEmpty);

            RuleFor(r => r.PublishDate)
                .NotNull()
                .WithMessage(CustomMessages.NotEmpty)
                .Must(m => m.Date >= DateTime.Now.Date)
                .WithMessage(CustomMessages.MustGreaterThanOrEqualNow);
        }
    }
}