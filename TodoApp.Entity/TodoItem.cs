﻿using System;

namespace TodoApp.Entity
{
    public class TodoItem : EntityBase
    {
        public string Subject { get; set; }
        public string Detail { get; set; }
        public string HostAddress { get; set; }
        public DateTime PublishDate { get; set; }
    }
}