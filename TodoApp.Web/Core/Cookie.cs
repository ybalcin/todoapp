﻿using System;

namespace TodoApp.Web.Core
{
    public class Cookie
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime ExpireTime { get; set; }
    }
}