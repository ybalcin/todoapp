﻿using System;
using System.Web;
using System.Web.Mvc;
using FluentValidation;

namespace TodoApp.Web.Core
{
    public static class Extensions
    {
        public static ModelStateDictionary AddModelError(this ModelStateDictionary modelState, ValidationException ex)
        {
            foreach (var err in ex.Errors)
            {
                modelState.AddModelError(err.PropertyName, err.ErrorMessage);
            }

            return modelState;
        }

        public static string GetCookie(this HttpRequestBase request, string key)
        {
            return request.Cookies[key]?.Value;
        }

        public static DateTime SetZeroAfterMinutes(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, 0, 0);
        }
    }
}