﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using TodoApp.Business.Abstract;
using TodoApp.Dto;
using TodoApp.Web.Core;
using TodoApp.Web.Models;

namespace TodoApp.Web.Controllers
{
    public class TodoController : Controller
    {
        private readonly ITodoItemService _itemService;

        public TodoController(ITodoItemService itemService)
        {
            _itemService = itemService;
        }

        // GET
        public ActionResult Index()
        {
            var itemList = _itemService.GetList().ToList();
            var vm = new TodoIndexViewModel
            {
                ItemList = itemList
            };

            var popupIds = Request.GetCookie(CookieKeys.PopupKey);
            var popupIdList = string.IsNullOrEmpty(popupIds)
                ? new List<string>()
                : popupIds.Split(new[] {'%'}, StringSplitOptions.RemoveEmptyEntries).ToList();
            vm.PopupItem = itemList.FirstOrDefault(f =>
                f.PublishDate <= DateTime.Now.SetZeroAfterMinutes() && !popupIdList.Contains(f.Id.ToString()));

            return View(vm);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TodoItemDto dto)
        {
            try
            {
                dto.HostAddress = Request.UserHostAddress;
                _itemService.Add(dto);
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex);
                return View(dto);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var dto = _itemService.Get(id);
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TodoItemDto dto)
        {
            try
            {
                dto.HostAddress = Request.UserHostAddress;
                _itemService.Update(dto);
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex);
                return View(dto);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            _itemService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}