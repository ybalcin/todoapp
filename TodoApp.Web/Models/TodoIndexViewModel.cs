﻿using System.Collections.Generic;
using TodoApp.Dto;

namespace TodoApp.Web.Models
{
    public class TodoIndexViewModel
    {
        public TodoIndexViewModel()
        {
            ItemList = new List<TodoItemDto>();
        }
        public List<TodoItemDto> ItemList { get; set; }
        public TodoItemDto PopupItem { get; set; }
    }
}