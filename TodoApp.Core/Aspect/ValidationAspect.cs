﻿using System;
using System.Linq;
using Castle.DynamicProxy;
using FluentValidation;
using TodoApp.Core.CrossCuttingConcern.Validation;
using TodoApp.Core.Utility.Interceptor;

namespace TodoApp.Core.Aspect
{
    public class ValidationAspect : MethodInterception
    {
        private readonly Type _validatorType;

        public ValidationAspect(Type validatorValidatorType)
        {
            if (!typeof(IValidator).IsAssignableFrom(validatorValidatorType))
                throw new Exception("Wrong validation type!");

            _validatorType = validatorValidatorType;
        }

        protected override void OnBefore(IInvocation invocation)
        {
            var validator = (IValidator) Activator.CreateInstance(_validatorType);
            var entityType = _validatorType.BaseType.GetGenericArguments()[0];

            var entities = invocation.Arguments.Where(t => t.GetType() == entityType);
            foreach (var entity in entities)
            {
                ValidationTool.Validate(validator, entity);
            }
        }
    }
}